// JQuery ********************
$(document).ready(function () {

// Index page header
var $navigatorButtons = $(".top-header__options-navigator--btn");
var $navigatorButtonInvest = $("#btnInvest");
var $navigatorButtonLoan = $("#btnLoan");

var $indexHeaderOption1 = $(".top-header__option--1");
var $indexHeaderOption2 = $(".top-header__option--2");

var $loanImage = $("#loanImg");
var $investImage = $("#investImg");

$navigatorButtons.click(function () {
  $(this).siblings().removeClass("active");
  $(this).addClass("active");
});

$navigatorButtonInvest.click(function () {
  $indexHeaderOption1.fadeOut(300);
  $loanImage.fadeOut(300);
  $indexHeaderOption2.delay(300).fadeIn();
  $investImage.delay(300).fadeIn();
});

$navigatorButtonLoan.click(function () {
  $indexHeaderOption2.fadeOut(300);
  $investImage.fadeOut();
  $indexHeaderOption1.delay(300).fadeIn();
  $loanImage.delay(300).fadeIn();
});



// Faq
var $faqTypeToggler = $(".faq__type");
var $faqHeader = $(".faq__header");
var $faqContent = $(".faq__content");
var $faqIcon = $(".faq__toggle-icon");
var $faqIcons = $(".faq__toggle-icon span");
var $faqIconPlus = $(".faq__toggle-icon--plus");
var $faqIconMinus = $(".faq__toggle-icon--minus");

var $loanFaq = $("#loanFAQ");
var $investFaq = $("#investFAQ");
var $referFaq = $("#referFAQ");
var $loanFaqWrapper = $("#loanFaqWrapper");
var $investFaqWrapper = $("#investFaqWrapper");
var $referFaqWrapper = $("#referFaqWrapper");

$faqTypeToggler.click(function () {
  $(this).siblings().removeClass("active");
  $(this).addClass("active");
});

$faqHeader.click(function () {
  $(this).find($faqIcons).toggleClass("active");
  $(this).parent().siblings().find($faqIcons).not(this).toggleClass("active");
  $(this).parent().siblings().find($faqContent).not(this).slideUp(300);
  $(this).siblings($faqContent).slideToggle(300);
});

$loanFaq.click(function(){
  $loanFaqWrapper.siblings().fadeOut(200);
  $loanFaqWrapper.delay(200).fadeIn();
})

$investFaq.click(function(){
  $investFaqWrapper.siblings().fadeOut(200);
  $investFaqWrapper.delay(200).fadeIn();
})

$referFaq.click(function(){
  $referFaqWrapper.siblings().fadeOut(200);
  $referFaqWrapper.delay(200).fadeIn();
})



// Mobile naviator
var $openNavBtn = $(".mobile-nav__open-btn--wrapper");
var $closeNavBtn = $(".mobile-nav__close-btn");
var $mobilNav = $(".mobile-nav");

$openNavBtn.click(function () {
  $mobilNav.siblings().css({
    transform: "translate(-300px,200px)",
  });
  $mobilNav.css({
    transform: "translateX(-300px)",
  });
  $(this).css({
    opacity: 0,
  });

  console.log("DOne");
});

$closeNavBtn.click(function () {
  $mobilNav.siblings().css({
    transform: "translate(0,0)",
  });
  $mobilNav.css({
    transform: "translateX(0)",
  });
  $openNavBtn.css({
    opacity: 1,
  });
});

// Disclaimer
var $products = $("#products");
var $referals = $("#referals");
var $productsWrapper = $("#productsWrapper");
var $referalsWrapper = $("#referalsWrapper");

$products.click(function(){
  $productsWrapper.siblings().fadeOut(200);
  $productsWrapper.delay(200).fadeIn();
})

$referals.click(function(){
  $referalsWrapper.siblings().fadeOut(200);
  $referalsWrapper.delay(200).fadeIn();
})

// Top nav dropdown
var $dropdownWrapper = $(".dropdown");
var $menu = $(".has-dropdown");
var $dropdown = $(".dropdown__content");

$menu.click(function(e){
  e.preventDefault();
  $(this)
  .parent()
  .siblings($dropdownWrapper)
  .find($dropdown)
  .not(this)
  .removeClass("active");

  $(this)
  .siblings($dropdown)
  .toggleClass("active");
});

// Mobile nav dropdown
var $mobileDropdownWrapper = $(".mobile-dropdown");
var $mobileMenu = $(".mobile-dropdown__toggle");
var $mobileDropdown = $(".mobile-dropdown__content");

$mobileMenu.click(function(e){
  e.preventDefault();
  $(this)
  .parent()
  .siblings($mobileDropdownWrapper)
  .find($mobileDropdown)
  .not(this)
  .slideUp();

  $(this).toggleClass("active");

  $(this)
  .siblings($dropdown)
  .slideToggle();
});

});
